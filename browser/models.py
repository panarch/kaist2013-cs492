#-*- coding:utf-8 -*-
from django.db import models
from cs492.donation.models import *
# Create your models here.

'''
	게시판 용도의 DB
'''

'''
	다중 보드용 일단은 놔두고...
'''
class BoardCategory(models.Model):
	title = models.CharField(max_length = 50)
	pass

class Board(models.Model):

	'''
	계정 정보 추가, name 제거
	'''
	account = models.ForeignKey(Account)
	boardcategory = models.ForeignKey(BoardCategory)

	subject = models.CharField(max_length=50, blank=True)
	#name = models.CharField(max_length=50, blank=True)
	created_date = models.DateField(null=True, blank=True)
	modified_date = models.DateField(null=True, blank=True)
	#mail = models.CharField(max_length=50, blank=True)
	content = models.CharField(max_length=1000, blank=True)
	hits = models.IntegerField(null=True, blank=True)

class BoardComment(models.Model):
	board = models.ForeignKey(Board)
	account = models.ForeignKey(Account)
	content = models.CharField(max_length = 300)
	pass

class Message(models.Model):
	account = models.ForeignKey(Account)
	account_sender = models.ForeignKey(Account, related_name = 'message_set2')
	content = models.CharField(max_length = 300)
	created_date = models.DateField()
	confirmed = models.BooleanField(default = False)
	pass