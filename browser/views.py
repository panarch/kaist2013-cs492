#-*- coding:utf-8 -*-
from django.shortcuts import redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.template.context import Context, RequestContext
from django.template import Context, loader
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response

from cs492.donation.models import MALE, FEMALE, Teacher, Student, MatchQueue, MatchLog, MATCH_NONE, \
                                MATCH_ONGOING, MATCH_DONE, Account

from datetime import datetime, timedelta
from browser.models import Board, BoardCategory, BoardComment, Message
from django.contrib.auth.models import User

from django.contrib.auth import authenticate
from django.contrib.auth import login as django_login
from django.contrib.auth import logout
from django.db.models import Avg, Max, Count
from django.core import urlresolvers
from django.db.models import Q

def get_last_aspect(req, account = None):
	if not account:
		account = Account.objects.filter(user = req.user)[0]
		pass

	if account.teacher:
		url_temp_name = 'teacher'
		url_temp_id = account.teacher.id
	else:
		url_temp_name = 'student'
		url_temp_id = account.student.id

	num = Message.objects.filter(account = account, confirmed = False).count()

	return {'url_temp_name': url_temp_name, 'url_temp_id': url_temp_id, 'num_messages': num}


def login(req):
	response_data = {'no_auth': True, 'next': req.GET['next'] if req.GET else None}
	print 'why'
	if 'id' in req.REQUEST:
		id = req.REQUEST['id']
		pw = req.REQUEST['pw']

		account = Account.objects.filter(id = id, password = pw)
		if not account:
			c = Context(response_data)
			return render_to_response('login.html',c)
		account = account[0]
		print account
		if not account.user:
			#user = User(username = account.id, password = pw)
			#user.save()
			user = User.objects.create_user(account.id, 'test@test.com', pw)
			account.user = user
			account.save()
	


		print account.id, pw
		user = authenticate(username=account.id, password = pw)
		print user
		if not user:
			pass
		print user.is_active
		django_login(req, user)
		print 'login success'

		#print req.GET['next']
			
		if 'next' in req.REQUEST:
			return HttpResponseRedirect(req.REQUEST['next'])

		#return HttpResponseRedirect(request.META.get('HTTP_REFERER','/'))
		return redirect('status')

	c = Context(response_data)
	return render_to_response('login.html',c)
	

def logout_page(req):
	logout(req)

	return redirect('login')

@login_required
def status(req):

	#Get Realtime Member
	#Matched list
	#current_matched_list = MatchLog.objects.filter(datetime_end=None).order_by('-datetime_begin')
	current_matched_list = MatchLog.objects.filter(datetime_end=None, datetime_begin__gt=datetime.now() - timedelta(hours=1)).order_by('datetime_begin')
	#Matching list. Only teacher are here
	current_matching_list = MatchQueue.objects.filter(done=False, datetime_register__gt=datetime.now() - timedelta(hours=1)).order_by('-datetime_register')

	values = {
		'matched_list': current_matched_list,
		'matching_list':current_matching_list,
	}

	#Filtering
	if req.method == 'POST':
		filter_order = req.POST['order']
		filter_score = req.POST['score']
		filter_age = req.POST['age']
		filter_gender = req.POST['gender']
		filter_name = req.POST['name']

		values.update({
			'filter_order':filter_order,
			'filter_score':filter_score,
			'filter_age':filter_age,
			'filter_gender':filter_gender,
			'filter_name':filter_name,
		})


	teacher_list = Teacher.objects.order_by('name')
	student_list = Student.objects.order_by('name')
	
	
	template = loader.get_template('status.html')
	values.update({
		'teacher_list': teacher_list,
		'student_list': student_list,
	})
	
	values.update(get_last_aspect(req))

	if 'student_tab' in req.GET:
		values['student_active'] = True
	#print values['url_temp_name']
	context = Context(values)
	return HttpResponse(template.render(context))

	#return HttpResponse(output)
	#return HttpResponse("Hellow, world")


@login_required
def detail(req, t_type, t_id):
	account = None
	response_data = None
	if t_type == 'teacher':
		teacher = Teacher.objects.get(pk=t_id)
		history = MatchLog.objects.filter(teacher=teacher)
		template = loader.get_template('info.html')
		response_data = {
			'type': t_type,
			'teacher': teacher,
			'history' : history,
		}
		account = Account.objects.filter(teacher = teacher)
	elif t_type == 'student':
		student = Student.objects.get(pk=t_id)
		history = MatchLog.objects.filter(student=student)
		template = loader.get_template('info.html')
		response_data ={
		'type': t_type,
		'student': student,
		'history' : history,
		}
		account = Account.objects.filter(student = student)
	else:
		return HttpResponse("Wrong Parameter")

	if not account:
		response_data['messages'] = []
		response_data.update(get_last_aspect(req))
		context = Context(response_data)
		return HttpResponse(template.render(context))
	else:
		account = account[0]


	account_mine = Account.objects.filter(user = req.user)[0]
	if 'content' in req.POST:
		content = req.POST['content']
		#print content
		m = Message(account = account, account_sender = account_mine,
					content = content, created_date= datetime.now())
		m.save()
		pass

	'''
	쪽지 리스트
	내꺼면 둘 다 불러오고 
	남의 프로필이면
	'''
	
	if account.id == account_mine.id:
		response_data['mine'] = True
		messages = Message.objects.filter(Q(account = account) | Q(account_sender = account))
	else:
		messages = Message.objects.filter(account_sender = account_mine)

	messages = messages.values('account__teacher__name', 'account__teacher__id',
					'account__student__name', 'account__student__id',
					'account_sender__teacher__name', 'account_sender__teacher__id',
					'account_sender__student__name', 'account_sender__student__id',
					'account_sender__id',
					'content', 'account__id', 'id', 'created_date', 'confirmed').order_by('-id')

	for m in messages:
		m['mine'] = True if m['account__id'] == account_mine.id else False
	response_data['messages'] = messages


	if account.id == account_mine.id:
		Message.objects.filter(account = account).update(confirmed = True) # 나에게 온걸 읽음으로,
		print 'confirmed'

	response_data['account_id'] = account_mine.id

	response_data.update(get_last_aspect(req))

	context = Context(response_data)
	return HttpResponse(template.render(context))


@login_required
def detail_delete(req, t_type, t_id):
	message_id = req.GET['messageid']
	Message.objects.filter(pk=message_id).delete()


	return HttpResponseRedirect('../')	

@login_required
def about(req):
	template = loader.get_template('about.html')
	context = Context({
	})
	return HttpResponse(template.render(context))

@login_required
def contact(req):
	template = loader.get_template('contact.html')
	context = Context({
	})
	return HttpResponse(template.render(context))

@login_required
def board(req, board_id = None):
	if not board_id:
		board_id = req.GET['boardid']
	else:
		board_id = boardid
	boardcategory = BoardCategory.objects.get(pk = board_id)
	boardList = Board.objects.annotate(Count('boardcomment'))\
					.values('boardcomment__count', 'account__id', 'id', 'created_date', 'hits', 'subject',
							'account__teacher__name', 'account__student__name', 'account__teacher__id', 'account__student__id')\
					.filter(boardcategory_id = board_id).order_by('-id')
	totalCnt = Board.objects.filter(boardcategory_id = board_id).count()


	#pagingHelperIns = pagingHelper();
    #totalPageList = pagingHelperIns.getTotalPageList( totalCnt, rowsPerPage)
    #print 'totalPageList', totalPageList


	template = loader.get_template('board.html')

	response_data = {
		'boardList': boardList,
		'totalCnt': totalCnt,
		'boardCategory': boardcategory
	}
	response_data.update(get_last_aspect(req))
	context = Context(response_data)
	return HttpResponse(template.render(context))

@login_required
def board_write(req):
	template = loader.get_template('write.html')
	account = Account.objects.filter(user = req.user)[0]
	boardcategory = BoardCategory.objects.get(pk= req.GET['boardid'])

	response_data = {
		'account': account,
		'boardCategory': boardcategory,
	}

	if 'id' in req.GET:
		board = Board.objects.get(pk=req.GET['id'])
		response_data['board'] = board
		pass

	response_data.update(get_last_aspect(req))
	context = Context(response_data)
	return HttpResponse(template.render(context))

@login_required
def board_delete(req):
	category_id = req.GET['boardid']
	board_id = req.GET['id']
	Board.objects.filter(pk = board_id).delete()

	return HttpResponseRedirect('../?boardid=%s' % category_id)

@login_required
def board_write_confirm(req):
	br = Board (
		subject = req.POST['subject'],
		account_id = req.POST['account_id'],
		boardcategory_id = req.POST['board_id'],
		#name = req.POST['name'],
		#mail = req.POST['email'],
		content = req.POST['content'],
		created_date = datetime.now(),
		modified_date = datetime.now(),
		hits = 0
	)
	if 'id' in req.POST:
		br.id = req.POST['id']
	br.save()
	#print req.POST['name'],
	#return HttpResponseRedirect(url)

	#url = '%s?%s' % (redirect('board'), 'boardid=1')
	#print url

	return HttpResponseRedirect('../?boardid=%s' % req.POST['board_id'])#urllib.urlencode(form.cleaned_data))

	#redirect('board', board_id=1)

@login_required
def board_view_content(req):
	pk= req.GET['content_id']
	boardData = Board.objects.get(id=pk)

	account = Account.objects.filter(user = req.user)[0]

	# 조회수를 늘린다.
	Board.objects.filter(id=pk).update(hits = boardData.hits + 1)

	if 'comment' in req.POST:
		comment = BoardComment(board_id = pk, account_id = req.POST['account_id'], content = req.POST['comment'])
		comment.save()
		pass

	response_data = {
		#'content_id': req.GET['content_id'],
		#'current_page':req.GET['current_page'],
		#'searchStr': req.GET['searchStr'],
		'boardData': boardData
	}

	comments = BoardComment.objects.values('id','account__id','content', 
		'account__teacher__name', 'account__student__name',
		'account__teacher__id', 'account__student__id').filter(board_id = pk)
	for comment in comments:
		if comment['account__id'] == account.id:
			comment['mine'] = True
			pass
		pass
	response_data['comments'] = comments
	response_data['account_id'] = account.id
	response_data.update(get_last_aspect(req))

	template = loader.get_template('view.html')
	context = Context(response_data)

	return HttpResponse(template.render(context))

@login_required
def board_view_comment_delete(req):
	comment_id = req.GET['commentid']
	BoardComment.objects.filter(pk = comment_id).delete()

	return HttpResponseRedirect('../../?content_id=%s' % req.GET['boardid'])




