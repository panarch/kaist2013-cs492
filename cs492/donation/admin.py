from django.contrib import admin
from cs492.donation.models import Teacher, Student, MatchQueue, MatchLog, Account

class TeacherAdmin(admin.ModelAdmin):
	pass
#list_display = ('first_name', 'last_name', 'email')

admin.site.register(Teacher)
admin.site.register(MatchQueue)
admin.site.register(Student)
admin.site.register(MatchLog)
admin.site.register(Account)
#admin.site.register(Author, AuthorAdmin)
#admin.site.register(Book)