#-*- coding:utf-8 -*-
from django.db import models
from django.contrib.auth.models import User


class Test(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)


MALE = 1
FEMALE = 2

class Student(models.Model):
    name = models.CharField(max_length = 20)
    age = models.IntegerField()
    gender = models.IntegerField(default=FEMALE)
    phone = models.CharField(max_length = 20)


    # 공부한 총 횟수, 시작, 끝
    #num_study_begin = models.IntegerField(default = 0)
    #num_study_end = models.IntegerField(default = 0)
    def __unicode__(self):
        return 'id : %d, name : %s' % (self.id, self.name)

    pass

class Teacher(models.Model):
    name = models.CharField(max_length = 20)
    email = models.EmailField(unique = True)
    age = models.IntegerField()
    gender = models.IntegerField(default=FEMALE)
    phone = models.CharField(max_length = 20)

    # 가르친 총 횟수
    #num_teach_begin = models.IntegerField(default = 0)
    #num_teach_end = models.IntegerField(default = 0)

    def __unicode__(self):
        return 'id : %d, name : %s' % (self.id, self.name)
    
    pass

class Account(models.Model):
    id = models.CharField(max_length=20, unique = True, primary_key = True)
    password = models.CharField(max_length=255) # password 처리해서 hash 값 저장
    student = models.ForeignKey(Student, null=True)
    teacher = models.ForeignKey(Teacher, null=True)
    user = models.OneToOneField(User, null=True)
    pass

MATCH_NONE = 0
MATCH_ONGOING = 1
MATCH_DONE = 2

'''
    match 성시시 MatchLog생성 후 아이디를 각각에게 보낸다.
'''
class MatchLog(models.Model):
    student = models.ForeignKey(Student, null=False)
    teacher = models.ForeignKey(Teacher, null=False)

    content_name = models.IntegerField(null = True) # Student가 신청시 넣을 컨텐츠 이름
    #content = models.ForeignKey(Content)

    datetime_begin = models.DateTimeField(auto_now_add = True)
    datetime_end = models.DateTimeField()
    # review가 끝나면 done True로,
    # 로그 남겨두면 각 student 혹은 Teacher에 대해
    # 얼마나 끝냈는지 등등 기록 확인 가능
    status_student = models.IntegerField(default = MATCH_ONGOING)
    status_teacher = models.IntegerField(default = MATCH_NONE)

    comment_student = models.TextField(null = True)
    comment_teacher = models.TextField(null = True)

    score_student = models.IntegerField(default = 0)
    score_teacher = models.IntegerField(default = 0)

    def __unicode__(self):
        return '%d (sid : %d) (tid : %d) %s' % \
            (self.id, self.student_id, self.teacher_id, str(self.datetime_begin))
 
    pass


class MatchQueue(models.Model):
    # Teacher가 처음으로 match 등록
    teacher = models.ForeignKey(Teacher, null=False)
    datetime_register = models.DateTimeField(auto_now_add = True)
    
    # Queue에서 datetime_limit기준으로 student가 match를 찾을 예정
    datetime_limit = models.DateTimeField(db_index = True)

    # 이것도 index 걸어야 한다. 근데 index걸만큼의 queue가 쌓일진 모르겠는데 일단 
    # 걸려면 수동으로 이것저것 바꿔줘야 해서 잠시 놔두고 나중에 걸도록 하자.
    done = models.BooleanField(default = False)

    match_log = models.ForeignKey(MatchLog, null = True)
    pass


'''
# 상황? 일상 대화, 등등
class Category(models.Model):
    title = models.CharField(max_length = 50)
    description = models.CharField(max_length = 200)
    pass

class Content(models.Model):
    category = models.ForeignKey(Category)
    title = models.CharField(max_length = 50)
    level = models.IntegerField(default = 1)
    description = models.CharField(max_length = 200)
    filename = models.CharField(max_length = 30)
    pass
'''

