#-*- coding:utf-8 -*-
from django.http import HttpResponse
import json
from django.template.context import Context, RequestContext
from cs492.donation.models import MALE, FEMALE, Teacher, Student, MatchQueue, MatchLog, MATCH_NONE, \
                                MATCH_ONGOING, MATCH_DONE, Account
from django.contrib.auth.models import User
import datetime


def home(req):

    response_data = {'status': 'success_server_unixtime', 'server_unixtime': 11.01}
    return HttpResponse(json.dumps(response_data), mimetype='application/json')

def account_login(req):
    account = Account.objects.filter(pk=req.REQUEST['id'])
    if not account:
        response_data = {'status': 'failed_wrong_id'}
        return HttpResponse(json.dumps(response_data), mimetype='application/json')
    
    account = account[0]
    if account.password != req.REQUEST['pw']:
        response_data = {'status': 'failed_wrong_password'}
        return HttpResponse(json.dumps(response_data), mimetype='application/json')

    current_matched_count = MatchLog.objects.filter(datetime_end=None, datetime_begin__gt=datetime.datetime.now() - datetime.timedelta(hours=1)).count()
    #Matching list. Only teacher are here
    current_matching_count = MatchQueue.objects.filter(done=False, datetime_register__gt=datetime.datetime.now() - datetime.timedelta(hours=1)).count()

    response_data = {'status':'success_account_login', 'account_id': account.id,
                        'num_active_teacher': current_matched_count + current_matching_count}
    if account.teacher:
        teacher = account.teacher
        response_data['teacher'] = {
            'id': teacher.id,
            'email': teacher.email,
            'name': teacher.name,
            'age': teacher.age,
            'gender': 'male' if teacher.gender == MALE else 'female',
            'phone': teacher.phone
        }
    elif account.student:
        student = account.student
        response_data['student'] = {
            'id': student.id,
            'name': student.name,
            'age': student.age,
            'gender': 'male' if student.gender == MALE else 'female',
            'phone': student.phone,
        }
    return HttpResponse(json.dumps(response_data), mimetype='application/json')
    

def teacher_register(req):
    print 'get', req.GET
    print req.POST
    name = req.POST['name']
    age = int(req.POST['age'])
    email = req.POST['email']
    gender = MALE if req.POST['gender'] == 'male' else FEMALE
    phone = req.POST['phone']

    account_id = req.POST['account_id']
    password = req.POST['pw']

    if Account.objects.filter(pk = account_id):
        response_data = {'status': 'failed_id_already_exists'}
        return HttpResponse(json.dumps(response_data), mimetype='application/json')

    if Teacher.objects.filter(email = email):
        response_data = {'status': 'failed_email_duplicated'}
        return HttpResponse(json.dumps(response_data), mimetype='application/json')
    teacher = Teacher(name = name, age = age, gender = gender, phone = phone, email = email)
    teacher.save()

    #user = User()
    #user.save()

    user = User.objects.create_user(account_id, email, password)
    user.save()
    
    account = Account(id = account_id, password = password, teacher = teacher)
    account.user = user
    account.save()
    teacher_info = {
        'account_id': account.id,

        'id': teacher.id,
        'email': teacher.email,
        'name': teacher.name,
        'age': teacher.age,
        'gender': 'male' if teacher.gender == MALE else 'female',
        'phone': teacher.phone,
    }

    response_data = {'status': 'success_teacher_register', 'teacher': teacher_info}
    return HttpResponse(json.dumps(response_data), mimetype='application/json')


def teacher_match_make(req):
    teacher = Teacher.objects.filter(pk=req.GET['tid'])
    if not teacher:
        response_data = {'status': 'failed_teacher_does_not_exists'}
        return HttpResponse(json.dumps(response_data), mimetype='application/json')        

    teacher = teacher[0]

    time = int(req.GET['time'])

    datetime_limit = datetime.datetime.now() + datetime.timedelta(minutes=time)
    queue = MatchQueue(teacher = teacher, datetime_limit = datetime_limit)
    queue.save()

    response_data = {'status': 'success_teacher_match_make', 'queue_id': queue.id}
    return HttpResponse(json.dumps(response_data), mimetype='application/json')

def teacher_match_delete(req):
    teacher = Teacher.objects.filter(pk=req.GET['tid'])
    if not teacher:
        response_data = {'status': 'failed_teacher_does_not_exists'}
        return HttpResponse(json.dumps(response_data), mimetype='application/json')

    teacher = teacher[0]
    MatchQueue.objects.filter(done=False).filter(teacher=teacher)\
        .filter(datetime_limit__gte=datetime.datetime.now()).update(datetime_limit=datetime.datetime(1989,6,11))

    response_data = {'status': 'success_match_delete'}
    return HttpResponse(json.dumps(response_data), mimetype='application/json')




# 아아 queue에 적어야 하려나? 보유하고 있는 queue id를 가지고 match_log_id를 구해야 하겠다.
def teacher_match_join(req):
    teacher = Teacher.objects.filter(pk=req.GET['tid'])
    if not teacher:
        response_data = {'status': 'failed_teacher_does_not_exists'}
        return HttpResponse(json.dumps(response_data), mimetype='application/json')        

    teacher = teacher[0]

    queue_id = req.GET['qid']
    queue = MatchQueue.objects.filter(pk = queue_id)
    if not queue:
        response_data = {'status': 'failed_queue_does_not_exists'}
        return HttpResponse(json.dumps(response_data), mimetype='application/json')                

    queue = queue[0]
    #match_id = req.GET['mid']
    #match_log = MatchLog.objects.filter(pk = match_id)
    match_log = queue.match_log
    if not match_log:
        response_data = {'status': 'failed_match_does_not_exists'}
        return HttpResponse(json.dumps(response_data), mimetype='application/json')

    match_log.status_teacher = MATCH_ONGOING
    match_log.save()

    student = match_log.student
    student_info = {
        'id': student.id,
        'name': student.name,
        'age': student.age,
        'gender': 'male' if student.gender == MALE else 'female',
        'phone': student.phone
    }

    account_temp = student.account_set.values_list('id')
    if account_temp:
        account_id = account_temp[0][0]
    else:
        account_id = None

    if account_id:
        student_info['account_id'] = account_id

    response_data = {'status': 'success_teacher_match_join', 'match_id': match_log.id,
                     'content_name': match_log.content_name,
                     'student': student_info}
    return HttpResponse(json.dumps(response_data), mimetype='application/json')


def teacher_match_finish(req):
    match_id = req.GET['mid']
    match_log = MatchLog.objects.filter(pk = match_id)
    if not match_log:
        response_data = {'status': 'failed_match_does_not_exists'}
        return HttpResponse(json.dumps(response_data), mimetype='application/json')
    match_log = match_log[0]

    match_log.status_teacher = MATCH_DONE
    match_log.comment_teacher = req.REQUEST['comment']
    if 'score' in req.REQUEST:
        match_log.score_teacher = int(req.REQUEST['score'])
    match_log.datetime_end = datetime.datetime.now()
    match_log.save()

    response_data = {'status': 'success_teacher_match_finish'}
    return HttpResponse(json.dumps(response_data), mimetype='application/json')

'''
    가르쳤던 학생들 정보를 보여주는 API
'''
def teacher_history_student(req):
    teacher = Teacher.objects.filter(pk=req.GET['tid'])
    if not teacher:
        response_data = {'status': 'failed_teacher_does_not_exists'}
        return HttpResponse(json.dumps(response_data), mimetype='application/json')

    teacher = teacher[0]
    history = []
    used_id = set()
    history_raw = MatchLog.objects.values('student__name', 'student__id', 'student__age', 'student__gender').filter(teacher=teacher)
    for item in history_raw:
        if item['student__id'] not in used_id:
            used_id.add(item['student__id'])
            item['student__gender'] = 'male' if item['student__gender'] == MALE else 'female'
            history.append(item)
            pass
        pass
    response_data = {'status': 'success_teacher_history_student', 'history': history}

    return HttpResponse(json.dumps(response_data), mimetype='application/json')

def teacher_history_student_info(req):
    teacher = Teacher.objects.get(pk=req.GET['tid'])
    student = Student.objects.get(pk=req.GET['sid'])
    history = MatchLog.objects.values().filter(student=student).filter(teacher=teacher)
    for item in history:
        del item['student_id']
        del item['teacher_id']
        item['datetime'] = str(item['datetime_begin'])
        del item['datetime_begin']
        del item['datetime_end']
        item['status_student'] = 'done' if item['status_student'] == MATCH_DONE else 'undone'
        item['status_teacher'] = 'done' if item['status_teacher'] == MATCH_DONE else 'undone'
        print item
        
        pass
    history = list(history)

    student_info = {
        'id': student.id,
        'name': student.name,
        'age': student.age,
        'gender': 'male' if student.gender == MALE else 'female'
    }
    response_data = {'status': 'success_teacher_history_student_info', 'student_history': history, 'student': student_info}
    return HttpResponse(json.dumps(response_data), mimetype='application/json')


'''
    가르쳤던 컨텐츠 정보를 보여주는 API
    각 컨텐츠별로 학생들에게 받은 코멘트들을 볼 수 있다.
'''
def teacher_history_content(req):
    teacher = Teacher.objects.filter(pk=req.GET['tid'])
    if not teacher:
        response_data = {'status': 'failed_teacher_does_not_exists'}
        return HttpResponse(json.dumps(response_data), mimetype='application/json')

    teacher = teacher[0]

    history = MatchLog.objects.values('id', 'student__name', 'student__id', 'student__age', 'student__gender', \
                'datetime_begin', 'score_student', 'comment_student', 'content_name').filter(teacher=teacher).order_by('-datetime_begin')
    for item in history:
        item['datetime'] = str(item['datetime_begin'])
        del item['datetime_begin']
        item['student__gender'] = 'male' if item['student__gender'] == MALE else 'female'
        pass

    history = list(history)
    response_data = {'status': 'success_teacher_history_content', 'history': history}

    return HttpResponse(json.dumps(response_data), mimetype='application/json')

'''
def teacher_history_content_info(req):
    teacher = Teacher.objects.get(pk=req.GET['tid'])
    content_name = req.GET['cname']
    content_name = MatchLog.objects.filter(pk = req.GET['lid']).filter(content_name = content_name)
    if not matchlog:
        response_data = {'status': 'failed_content_does_not_exists'}
        return HttpResponse(json.dumps(response_data), mimetype='application/json')

    matchlog = matchlog[0]

    pass
'''

def student_register(req):

    '''
    name = models.CharField(max_length = 20)
    age = models.IntegerField()
    gender = models.IntegerField(default=FEMALE)
    phone = models.CharField(max_length = 20)
    '''
    name = req.POST['name']
    age = int(req.POST['age'])
    gender = MALE if req.POST['gender'] == 'male' else FEMALE
    phone = req.POST['phone']

    account_id = req.POST['account_id']
    password = req.POST['pw']

    if Account.objects.filter(pk = account_id):
        response_data = {'status': 'failed_id_already_exists'}
        return HttpResponse(json.dumps(response_data), mimetype='application/json')

    student = Student(name = name, age = age, gender = gender, phone = phone)
    student.save()

    account = Account(id = account_id, password = password, student = student)
    account.save()

    student_info = {
        'account_id': account.id,

        'id': student.id,
        'name': student.name,
        'age': student.age,
        'gender': 'male' if student.gender == MALE else 'female',
        'phone': student.phone,
    }

    response_data = {'status': 'success_student_register', 'student': student_info}
    return HttpResponse(json.dumps(response_data), mimetype='application/json')


def student_match_find(req):
    content_name = req.GET['cname']
    student = Student.objects.filter(pk=req.GET['sid'])
    if not student:
        response_data = {'status': 'failed_student_does_not_exists'}
        return HttpResponse(json.dumps(response_data), mimetype='application/json')

    student = student[0]

    queue = MatchQueue.objects.filter(done=False).filter(datetime_limit__gte=datetime.datetime.now()).order_by('?')
    if not queue:
        response_data = {'status': 'failed_no_teacher_to_match'}
        return HttpResponse(json.dumps(response_data), mimetype='application/json')
    queue = queue[0]

    queue.done = True

    teacher = queue.teacher
    match_log = MatchLog(student = student, teacher = teacher,
                         content_name = content_name)
    match_log.save()

    queue.match_log = match_log
    queue.save()

    account_temp = teacher.account_set.values_list('id')
    if account_temp:
        account_id = account_temp[0][0]
    else:
        account_id = None
    teacher_info = {
        'id': teacher.id,
        'name': teacher.name,
        'phone': teacher.phone,
        'gender': 'male' if teacher.gender == MALE else 'female'
    }
    if account_id:
        teacher_info['account_id'] = account_id

    response_data = {'status': 'success_student_match_find', 'match_id': match_log.id,
                     'teacher': teacher_info}
    return HttpResponse(json.dumps(response_data), mimetype='application/json')
    
def student_match_finish(req):
    match_id = req.GET['mid']
    match_log = MatchLog.objects.filter(pk = match_id)
    if not match_log:
        response_data = {'status': 'failed_match_does_not_exists'}
        return HttpResponse(json.dumps(response_data), mimetype='application/json')
    match_log = match_log[0]

    match_log.status_student = MATCH_DONE
    match_log.comment_student = req.REQUEST['comment']
    if 'score' in req.REQUEST:
        match_log.score_student = int(req.REQUEST['score'])
    match_log.datetime_end = datetime.datetime.now()
    match_log.save()

    response_data = {'status': 'success_student_match_finish'}
    return HttpResponse(json.dumps(response_data), mimetype='application/json')

'''
    자신이 공부했던 컨텐츠 리스트 반환?
'''
def student_history_content(req):
    student = Student.objects.filter(pk=req.GET['sid'])
    if not student:
        response_data = {'status': 'failed_student_does_not_exists'}
        return HttpResponse(json.dumps(response_data), mimetype='application/json')

    student = student[0]

    history = MatchLog.objects.values('id', 'teacher__name', 'teacher__id', 'teacher__age', 'teacher__gender', \
                'datetime_begin', 'comment_teacher', 'score_student', 'score_teacher',
                'comment_student', 'content_name').filter(student = student).order_by('-datetime_begin')
    for item in history:
        item['datetime'] = str(item['datetime_begin'])
        del item['datetime_begin']
        item['teacher__gender'] = 'male' if item['teacher__gender'] == MALE else 'female'
        pass

    history = list(history)
    response_data = {'status': 'success_student_history_content', 'history': history}

    return HttpResponse(json.dumps(response_data), mimetype='application/json')    




