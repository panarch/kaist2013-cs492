from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'cs492.donation.views.home', name='home'),
    url(r'^teacher/register/', 'cs492.donation.views.teacher_register', name='teacher_register'),
    url(r'^teacher/match/make/', 'cs492.donation.views.teacher_match_make', name='teacher_match_make'),
    url(r'^teacher/match/delete/', 'cs492.donation.views.teacher_match_delete', name='teacher_match_delete'),
    url(r'^teacher/match/join/', 'cs492.donation.views.teacher_match_join', name='teacher_match_join'),
    url(r'^teacher/match/finish/', 'cs492.donation.views.teacher_match_finish', name='teacher_match_finish'),
    url(r'^teacher/history/student/info/', 'cs492.donation.views.teacher_history_student_info', name='teacher_history_student_info'),
    url(r'^teacher/history/student/', 'cs492.donation.views.teacher_history_student', name='teacher_history_student'),
    url(r'^teacher/history/content/', 'cs492.donation.views.teacher_history_content', name='teacher_history_content'),


    url(r'^student/register/', 'cs492.donation.views.student_register', name='student_register'),
    url(r'^student/match/find/', 'cs492.donation.views.student_match_find', name='student_match_find'),
    url(r'^student/match/finish/', 'cs492.donation.views.student_match_finish', name='student_match_finish'),
    url(r'^student/history/content/', 'cs492.donation.views.student_history_content', name='student_history_content'),

    url(r'^account/login/', 'cs492.donation.views.account_login', name='account_login'),

    # django login
    url(r'^accounts/login/', 'browser.views.login', name='login_a'),

    url(r'^accounts/logout/', 'browser.views.logout_page', name='logout'),
    
    #Url for status browser
    url(r'^status/$', 'browser.views.status', name='status'),
    
    url(r'^status/(?P<t_type>student)/(?P<t_id>\d+)/delete/', 'browser.views.detail_delete', name='detail_delete'),
    url(r'^status/(?P<t_type>teacher)/(?P<t_id>\d+)/delete/', 'browser.views.detail_delete', name='detail_delete'),
    url(r'^status/(?P<t_type>teacher)/(?P<t_id>\d+)/', 'browser.views.detail', name='detail'),
    url(r'^status/(?P<t_type>student)/(?P<t_id>\d+)/', 'browser.views.detail', name='detail'),


    url(r'^status/login/$', 'browser.views.login', name='login'),

    url(r'^status/about/', 'browser.views.about', name='about'),
    url(r'^status/contact/', 'browser.views.contact', name='contact'),
    url(r'^status/board/$', 'browser.views.board', name='board'),
    url(r'^status/board/delete/$', 'browser.views.board_delete', name='board_delete'),
    url(r'^status/board/write/$', 'browser.views.board_write', name='board_write'),
    url(r'^status/board/write/confirm', 'browser.views.board_write_confirm', name='board_write_confirm'),
    url(r'^status/board/view/$', 'browser.views.board_view_content', name='board_view_content'),
    url(r'^status/board/view/comment/delete/$', 'browser.views.board_view_comment_delete', name='comment_delete'),


    # url(r'^cs492/', include('cs492.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
